<?php
	session_start();
?>
<!DOCTYPE HTML>
<html lang="en">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="writestyle.css" type="text/css" />
</head>
<body>
	<?php
		require "header.php";
		require "mainbackground.php";
	?>
	<div class="main">
	
		<div class="leftbar">

            <input class="title" placeholder=""></input>
            <div class="content">
                <div class="imgborder"><div class="img"><i class="icon-picture-1"></i></div></div>
                <textarea class="intro"></textarea>

            </div>
            <textarea class="essay" name="essay" id=""></textarea>
			
		</div>
		<?php
			require "rightbar.php";
		?>
	</div>
</body>

</html>
