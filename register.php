<?php
	session_start();
?>

<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="registerstyle.css" type="text/css" />
</head>

<body>
	
<div id="container">
	<div class="logo">
		<a href="mplogin.php"><img src="logook.png" /></a>
	</div>
	<form action="includes/signup.inc.php" method="post">
		<div class="message">Register:</div>
		<input type="text" name="mail" onfocus="this.placeholder=''" onblur="this.placeholder='mail'" placeholder="mail" value="<?php 
			if(isset($_SESSION['incorectuser'])) {
				echo $_SESSION['fr_mail'];
				unset($_SESSION['fr_mail']);
			}
			else if(isset($_SESSION['pwdcheckunsuc'])) {
				echo $_SESSION['fr_mail'];
				unset($_SESSION['fr_mail']);
			}
			else if(isset($_SESSION['usertaken'])) {
				echo $_SESSION['fr_mail'];
				unset($_SESSION['fr_mail']);
			}
			else if(isset($_SESSION['emptyuid'])) {
				echo $_SESSION['fr_mail'];
				unset($_SESSION['fr_mail']);
			}
			else if(isset($_SESSION['emptypassword2'])) {
				echo $_SESSION['fr_mail'];
				unset($_SESSION['fr_mail']);
			}
			else{
				echo'';
			}
		?>" >
		<input type="text" name="uid" onfocus="this.placeholder=''" onblur="this.placeholder='user name'" placeholder="user name" value="<?php 
			if(isset($_SESSION['incorectmail'])) {
				echo $_SESSION['fr_nick'];
				unset($_SESSION['fr_nick']);
			}
			else if(isset($_SESSION['pwdcheckunsuc'])) {
				echo $_SESSION['fr_nick'];
				unset($_SESSION['fr_nick']);
			}
			else if(isset($_SESSION['mailtaken'])) {
				echo $_SESSION['fr_nick'];
				unset($_SESSION['fr_nick']);
			}
			else if(isset($_SESSION['emptymail'])) {
				echo $_SESSION['fr_nick'];
				unset($_SESSION['fr_nick']);
			}
			else if(isset($_SESSION['emptypassword2'])) {
				echo $_SESSION['fr_nick'];
				unset($_SESSION['fr_nick']);
			}
			else{
				echo'';
			}?>" >
		<input type="password" name="pwd" onfocus="this.placeholder=''" onblur="this.placeholder='password'" placeholder="password">
			
		<input type="password" name="pwdCheck" onfocus="this.placeholder=''" onblur="this.placeholder='repeat password'" placeholder="repeat password">
		<input type="submit" name="signup-submit" value="Register">
		<div class="question"><p>Already have an account, want to <a href="login.php">login </a> ?</p></div>
		<div class="info">By registring you accept <a href="">our Terms</a> and state that you read our <a href="">Privacy Policy</a> and <a href="">Content Policy</a> and that you agree with it.</div>
		<?php
			/*Message about errors in signup.*/

			if(isset($_SESSION['emptyfields2'])) {
				echo'<p>Please fill all of the fields.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['incorectuserandmail'])) {
				echo'<p>User and mail are incorrect.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['incorectmail'])) {
				echo'<p>Mail adress is incorrect.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['incorectuser'])) {
				echo'<p>User name is incorrect.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['pwdcheckunsuc'])) {
				echo'<p>Your passwords do not match.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['usermailtaken'])) {
				echo'<p>User name and mail are taken.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['usertaken'])) {
				echo'<p>User name is taken.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['mailtaken'])) {
				echo'<p>Mail is already in use.</p>';
				session_destroy();
				exit();
			}
			else if(isset($_SESSION['succes'])) {
				echo'<p style="color: #42B21C;">Succes! We will send you an email with further instuctions.</p>';
				session_destroy();
				exit();
			}
			else{
				/*//echo'<p>Database error, please contact our customer service</p>';//
				//echo'<p>Database error. We are curently working on the problem</p>';//*/
				session_destroy();
				exit();
			}
		?>
	</form>
</div>
	
</body>

</html>
