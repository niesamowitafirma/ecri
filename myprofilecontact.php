<?php
	session_start();
	
	if (!isset($_SESSION['userinfo'])){
		header('Location: login.php');
		exit();
	} else{
        $userinfo = $_SESSION['userinfo'];
    }
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="myprofilecontactstyle.css" type="text/css" />
	<link rel="stylesheet" href="mynameaaboveprofilestyle.css" type="text/css" />
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
	?>
	<div class="main">
	
		<div class="leftbar">
			<?php
				require "profileheader.php";
			?>
			<div class="content">
				<div class="bottomprofile">
                    <div class="namelike">
                        <div class="name">
                       <?php
                        if($userinfo['name']!=""){
                            echo $userinfo['name']; 
                        }else{
                            echo "Name and Surname";
                        }   
                        ?>
                        </div>
                        <div class="update">
                            <a href="profilecontactform.php"><button >Update</button></a>
                        </div>
                    </div>
					<div class="date">joined: <?php echo $userinfo['date']; ?></div>
					<div class="linksandvery">
						<div class="nocaptcha">
							<div class="contact"><?php
                            if($userinfo['contactmail']!=""){
                                echo "CONTACT: ".$userinfo['contactmail'];
                            } else{
                                
                            }
                            ?></div>
							<div class="info">LINKS:</div>
							<div class="info"><a href="<?php 
                                if($userinfo['link1']!=""){
                                    echo $userinfo['link1'];
                                } else{
                                    echo "";
                                } 
                                ?>" target="_blank"><?php 
                                if($userinfo['link1']!=""){
                                    echo $userinfo['linkname1'];
                                } else{
                                    echo "";
                                } 
                                ?></a></div>
							<div class="info"><a href="<?php 
                                if($userinfo['link2']!=""){
                                    echo $userinfo['link2'];
                                } else{
                                    echo "";
                                } 
                                ?>" target="_blank"><?php 
                                if($userinfo['link2']!=""){
                                    echo $userinfo['linkname2'];
                                } else{
                                    echo "";
                                } 
                                ?></a></div>
							<div class="info"><a href="<?php 
                                if($userinfo['link3']!=""){
                                    echo $userinfo['link3'];
                                } else{
                                    echo "";
                                } 
                                ?>" target="_blank"><?php 
                                if($userinfo['link3']!=""){
                                    echo $userinfo['linkname3'];
                                } else{
                                    echo "";
                                } 
                                ?></a></div>
							<div class="info"><a href="<?php 
                                if($userinfo['link4']!=""){
                                    echo $userinfo['link4'];
                                } else{
                                    echo "";
                                } 
                                ?>" target="_blank"><?php 
                                if($userinfo['link4']!=""){
                                    echo $userinfo['linkname4'];
                                } else{
                                    echo "";
                                } 
                                ?></a></div>
							<div class="info"><a href="<?php 
                                if($userinfo['link5']!=""){
                                    echo $userinfo['link5'];
                                } else{
                                    echo "";
                                } 
                                ?>" target="_blank"><?php 
                                if($userinfo['link5']!=""){
                                    echo $userinfo['linkname5'];
                                } else{
                                    echo "";
                                } 
                                ?></a></div>
							<div class="info"><a href="<?php 
                                if($userinfo['link6']!=""){
                                    echo $userinfo['link6'];
                                } else{
                                    echo "";
                                } 
                                ?>" target="_blank"><?php 
                                if($userinfo['link6']!=""){
                                    echo $userinfo['linkname6'];
                                } else{
                                    echo "";
                                } 
                                ?></a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>