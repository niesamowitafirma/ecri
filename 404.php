<?php
	session_start();
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="404style.css" type="text/css" />
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
	?>
	<div class="main">
	
		<div class="leftbar">
            <h3>Unfortunetly the page you are looking for, does not exist...</h3>
            <h5>Try <a href="mplogin.php">going to our homepage</a></h5>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>