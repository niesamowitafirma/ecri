<head>
	<link rel="stylesheet" href="headerstyle.css" type="text/css" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
	<div class="header">
        <div class="searchbar-mobile">
            <div class="navbar-nav-collapse collapse">
                <form action="searchresult.php" method="POST">
                    <input type="text" placeholder="search..." >
                    <button type="button" class="navbbar-toggle" data-toggle="collapse" data-target=".navbar-nav-collapse">x</button>
                </form>
            </div>
        </div>
        <div class="wrapper">
            <a href="mplogin.php"><div class="logo_container">
                <img src="logook2.png" />
            </div></a>
            <a href="mplogin.php"><div class="logo_containersmall">
                <img  src="logo2a2.png" />
            </div></a>
            <div class="searchandbutton">
               <form action="searchresult.php" method="POST">
                    <input type="text" placeholder="search..." >
                    <button type="submit"><i class="icon-search"></i></button>
                </form>
            </div>
            <ul class="navigation">
                <li class="navbbar-toggle" data-toggle="collapse" data-target=".navbar-nav-collapse"><a href="#"><i class="icon-search"></i></a></li>
                <?php
                    if(isset($_SESSION['userinfo'])) {	
                        echo  '<li><a href="myprofile.php"><i class="icon-user"></i></a></li>
                        <li><a href="includes/logout.inc.php"><i class="icon-logout"></i></a></li>';
                    }

                    else {
                        echo '<form action="login.php">
                            <div class="login">
                                <button name="login-button">LOG IN</button>
                            </div>
                        </form>';
                    }
                ?>
                <!-- navbar -->
                <div class="dropdown">
                    <button type="button" class="navbbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="icon-down-dir"></i></button>
                    <div class="navbar-collapse collapse">
                        <a href="#">Settings</a>
                        <a href="write.php">Write</a>
                        <a href="#">About</a>
                    </div>
                </div><!--  /navbar -->
            </ul>
        </div>
        <div class="mobile-dropdown">
            <ul class="navbar-collapse collapse">
                <li><a href="#">Settings</a></li>
                <li><a href="#">FAQ</a></li>
                <li><a href="#">About</a></li>
            </ul>		
        </div>
	</div>
</body>