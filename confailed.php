<?php
	session_start();
    if (!isset($_SESSION['confailed'])){
       header("Location: mplogin.php"); 
    }
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="404style.css" type="text/css" />
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
        $error = $_SESSION['confailed'];
	?>
	<div class="main">
	
		<div class="leftbar">
            <h3>Connection failed:</h3>
            <h5><?php echo $error; unset($_SESSION['confailed']);?></h5>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>