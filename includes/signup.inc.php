<?php
if (isset($_POST['signup-submit'])) {
	
	session_start();
	
	require 'dbh.inc.php';
	
	$username = $_POST['uid'];
	$passwordRepeat = $_POST['pwdCheck'];
	$email = $_POST['mail'];
	$password = $_POST['pwd'];

	$_SESSION['fr_nick'] = $username;
	$_SESSION['fr_mail'] = $email;
	
	//Various error sessions leading to the register.php and a correct info in  the header finished with a succesful register. 
	
	if (empty($username) || empty($passwordRepeat) || empty($email) || empty($password)) {
		$_SESSION['emptyfields2']=true;
		if (empty($username)){
			$_SESSION['emptyuid']=true;
			header("Location: ../register.php");
			exit();
		}
		else if (empty($email)){
			$_SESSION['emptymail']=true;
			header("Location: ../register.php");
			exit();
		}
		else{
			$_SESSION['emptypassword2']=true;
			header("Location: ../register.php");
			exit();
		}
	}
	else if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
		$_SESSION['incorectuserandmail']=true;
		header("Location: ../register.php");
		exit();
	}
	else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$_SESSION['incorectmail']=true;
		header("Location: ../register.php");
		exit();
	}
	else if (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
		$_SESSION['incorectuser']=true;
		header("Location: ../register.php");
		exit();
	}
	else if ($password !== $passwordRepeat){
		$_SESSION['pwdcheckunsuc']=true;
		header("Location: ../register.php");
		exit();
	}
	else {
		$sql = "SELECT uidUsers FROM users  WHERE uidUsers=? AND emailUsers=?";
		$stmt = mysqli_stmt_init($conn);
		if  (!mysqli_stmt_prepare($stmt, $sql)) {
			header("Location: ../register.php");
			exit();
		}
		else {
			mysqli_stmt_bind_param($stmt, "ss", $username, $email);
			mysqli_stmt_execute($stmt);
			mysqli_stmt_store_result($stmt);
			$resultCheck = mysqli_stmt_num_rows($stmt);
			if ($resultCheck > 0) {
				$_SESSION['usermailtaken']=true;
				header("Location: ../register.php");
				exit();
			}
			else {
				$sql = "SELECT uidUsers FROM users WHERE uidUsers=?";
				$stmt = mysqli_stmt_init($conn);
				if  (!mysqli_stmt_prepare($stmt, $sql)) {
					header("Location: ../register.php");
					exit();
				}
				else {
					mysqli_stmt_bind_param($stmt, "s", $username);
					mysqli_stmt_execute($stmt);
					mysqli_stmt_store_result($stmt);
					$resultCheck = mysqli_stmt_num_rows($stmt);
					if ($resultCheck > 0) {
						$_SESSION['usertaken']=true;
						header("Location: ../register.php");
						exit();
					}
					else {
						$sql = "SELECT emailUsers FROM users WHERE emailUsers=?";
						$stmt = mysqli_stmt_init($conn);
						if  (!mysqli_stmt_prepare($stmt, $sql)) {
							header("Location: ../register.php");
							exit();
						}
						else {
							mysqli_stmt_bind_param($stmt, "s", $email);
							mysqli_stmt_execute($stmt);
							mysqli_stmt_store_result($stmt);
							$resultCheck = mysqli_stmt_num_rows($stmt);
							if ($resultCheck > 0) {
								$_SESSION['mailtaken']=true;
								header("Location: ../register.php");
								exit();
							}
							else {
								$sql = "INSERT INTO users (uidUsers, emailUsers, pwdUsers) VALUES (?, ?, ?)";
								$stmt = mysqli_stmt_init($conn);
								if  (!mysqli_stmt_prepare($stmt, $sql)) {
									header("Location: ../register.php");
									exit();
								}
								else {
									$_SESSION['succes']=true;
									$hashedPwd = password_hash($password, PASSWORD_DEFAULT);
									
									mysqli_stmt_bind_param($stmt, "sss", $username, $email, $hashedPwd);
									mysqli_stmt_execute($stmt);
                                    mysqli_query($conn, "");
                                        
                                    $date = date('Y-m-d');
                                    mysqli_query($conn, "UPDATE users SET date='$date' WHERE emailUsers='$email'");
                                    
									header("Location: ../register.php");
									exit();
								}	
							}			
						}
					}
				}
			}
		}
	}
	mysqli_stmt_close($stmt);
	mysql_close($conn);
}
else{
	header("Location: ../register.php");
	exit();
}	