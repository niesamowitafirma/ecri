<?php
session_start();
require 'dbh.inc.php';

class Update{
    
    public function Inspect($link, $linkname, $i){
        
        require 'dbh.inc.php';

        $userinfo = $_SESSION['userinfo'];
        $uidUsers = $userinfo['uidUsers'];

        if(!empty($link) && empty($linkname)){     
            $file_headers = @get_headers($link);
            if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $_SESSION['wronglink'.$i] = $linkname; 
                
            }else{
                $_SESSION['templink'.$i] = $link; 
            }
        }else if(empty($link) && !empty($linkname)){

            $_SESSION['templinkname'.$i] = $linkname;
            header('Location: ../profilecontactform.php');
        }else if(!empty($link) && !empty($linkname)){
            //Checking if input is a link
            $file_headers = @get_headers($link);
            if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $_SESSION['wronglink'.$i] = $linkname; 
            }else {
                $query = "UPDATE users SET link$i=?, linkname$i=? WHERE uidUsers=?";
                //prepare query and execute
                if($stmt = $conn->prepare($query)){
                    $stmt->bind_param('sss', $link, $linkname, $uidUsers);
                    $stmt->execute();
                    $userinfo['link'.$i] = $link;
                    $userinfo['linkname'.$i] = $linkname;
                    $_SESSION['userinfo'] = $userinfo;
                }
            }
        }else if(empty($link) && empty($linkname)){
            $query = "UPDATE users SET link$i=?, linkname$i=? WHERE uidUsers=?";
            //prepare query and execute
            if($stmt = $conn->prepare($query)){
                $stmt->bind_param('sss', $link, $linkname, $uidUsers);
                $stmt->execute();
                $userinfo['link'.$i] = $link;
                $userinfo['linkname'.$i] = $linkname;
                $_SESSION['userinfo'] = $userinfo;
            }
        }
    }
    
    public function InspectMail($cmail){
        $userinfo = $_SESSION['userinfo'];
        $uidUsers = $userinfo['uidUsers'];
        $cmail = $_POST['contactmail'];
        
        if(!filter_var($cmail, FILTER_VALIDATE_EMAIL)){
            $_SESSION['wrongcontactmail'] = $cmail;
        }else{
           $query = "UPDATE users SET contactmail=? WHERE uidUsers=?";
            //prepare query and execute
            if($stmt = $conn->prepare($query)){
                $stmt->bind_param('ss', $cmail, $uidUsers);
                $stmt->execute();
                $_SESSION['correctontactmail'] = $cmail;
                $cmail = $_SESSION['correctontactmail'];
                $userinfo['contactmail'] = $cmail;
                $_SESSION['userinfo'] = $userinfo;
            }
        }
    }
}
    
if(isset($_POST['info-submit'])){
    $i=1;
    $cmail = $_POST['contactmail'];
    while($i <= 6){
       if(isset($_POST['linkname'.$i]) || isset($_POST['link'.$i])){
            $userinfo = $_SESSION['userinfo'];
            $uidUsers = $userinfo['uidUsers'];
            $linknamey = $_POST['linkname'.$i];
            $linky = $_POST['link'.$i];
            
            $inspecty = new Update();
            $inspecty->Inspect($linky, $linknamey, $i);
        }if(isset($_SESSION['wronglink'.$i]) || isset($_SESSION['templink'.$i]) || isset($_SESSION['templinkname'.$i])){
            $_SESSION["smthwrong"] = "smthwrong";
        }
        $i++;
    }if(!empty($cmail)){
        $inspectm = new Update();
        $inspectm->InspectMail($cmail);
    }if(!isset($_SESSION["smthwrong"])){
        header('Location: ../myprofilecontact.php');
        exit();
    }else{
        unset($_SESSION["smthwrong"]);
        header('Location: ../profilecontactform.php');
        exit();
    }
}

?>