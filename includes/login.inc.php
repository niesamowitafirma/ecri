<?php
if (isset($_POST['login-submit'])) {
	
	session_start();
	require 'dbh.inc.php';
	
	$mailuid = $_POST['mailuid'];
	$password = $_POST['pwd'];
	
	$_SESSION['fr_mailuid'] = $mailuid;
	$_SESSION['fr_nick'] = $username;
	
	if (empty($mailuid) || empty($password)) {
		$_SESSION['emptyfields1']=true;
		if (empty($mailuid)){
			$_SESSION['emptymailuid']=true;
			header("Location: ../login.php");
			exit();
		}
		else{
			$_SESSION['emptypassword1']=true;
			header("Location: ../login.php");
			exit();
		}
	}
	else {
		$sql = "SELECT * FROM users WHERE uidUsers=? OR emailUsers=?";
		$stmt = mysqli_stmt_init($conn);
		if (!mysqli_stmt_prepare($stmt, $sql)) {
			header("Location: ../login.php");
			exit();
		}
		else{
			
			mysqli_stmt_bind_param($stmt, "ss", $mailuid, $mailuid);
			mysqli_stmt_execute($stmt);
			$result = mysqli_stmt_get_result($stmt);
			
			if ($row = mysqli_fetch_assoc($result)) {
				$pwdCheck = password_verify($password, $row['pwdUsers']);
				if ($pwdCheck == false) {
					$_SESSION['wrongpwd']=true;
					header("Location: ../login.php");
					exit();
				}
				else if ($pwdCheck == true) {
                    
                    $selectinfo = "SELECT uidUsers, name, date, description, contactmail, link1, link2, link3, link4, link5, link6, linkname1, linkname2, linkname3, linkname4, linkname5, linkname6 FROM users WHERE uidUsers='$mailuid' OR emailUsers='$mailuid'";
                    
                    $userinfo = mysqli_query($conn, $selectinfo);
                    
                    $uiarray = array();
                    
                    $row = mysqli_fetch_assoc($userinfo);
                    $_SESSION['userinfo'] = $row;
                    
                    unset($_SESSION['fr_mailuid']);
                    unset($_SESSION['fr_nick']);
                    
					header("Location: ../mplogin.php");
				}
			}
			else{
				$_SESSION['nouser']=true;
				header("Location: ../login.php");
				exit();
			}
		}
	}
}

else{
	header("Location: ../login.php");
	exit();
}