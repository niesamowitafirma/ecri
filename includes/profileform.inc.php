<?php
session_start();
require 'dbh.inc.php';
	
if (isset($_POST['info-submit'])) {	
	$userinfo = $_SESSION['userinfo'];
	$name = $_POST['name_desc'];
	$desc = $_POST['description'];
    $useruid = $userinfo['uidUsers']; 
    
    $sql = "UPDATE users SET name=?, description=? WHERE uidUsers='$useruid'";
    $stmt = mysqli_stmt_init($conn);
    
    if(!mysqli_stmt_prepare($stmt, $sql)){
        header('Location: ../profileform.php');
        exit();
    } else{
        mysqli_stmt_bind_param($stmt, "ss", $desc, $name);
        mysqli_stmt_execute($stmt);
        $userinfo['name'] = $name;
        $userinfo['description'] = $desc;
        $_SESSION['userinfo'] = $userinfo;
        header('Location: ../myprofile.php');
        exit();
    }
    
    
} else{
    header('Location: ../myprofile.php');
    exit();
}
?>