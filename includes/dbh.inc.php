<?php

error_reporting(0);

$servername = "localhost";
$dBUsername = "root";
$dBPassword = "";
$dBName = "loginsystemtut";

$conn = mysqli_connect($servername, $dBUsername, $dBPassword, $dBName);

if (!$conn) {
    $_SESSION['confailed'] = mysqli_connect_error();
    header("Location: ../confailed.php");
    die();
}