<?php
	session_start();
	
	if (!isset($_SESSION['userinfo'])){
		header('Location: login.php');
		exit();
	} else{
        $userinfo = $_SESSION['userinfo'];
    }
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="myprofilestyle.css" type="text/css" />
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
	?>
	<div class="main">
	
		<div class="leftbar">
			<?php
				require "profileheader.php";
			?>
			<div class="content">
				<div class="bottomprofile">
					<?php
                        require "mynameaaboveprofile.php";
                    ?>
					<div class="uid">
						<?php		
                        echo $userinfo['uidUsers']; 
						?>
					</div>
					<div class="about">
                    <?php
                    
                if(!isset($userinfo['description'])){
                    echo 'Description - Here a person can write what ones field of intrest is, what one is likely to write about etc. All the other information like links to diffrent social media should be put in "contact". ';
                } else{
                    if($userinfo['description']!=""){
                        echo $userinfo['description'];
                    } else{
                        echo 'Description - Here a person can write what ones field of intrest is, what one is likely to write about etc. All the other information like links to diffrent social media should be put in "contact". ';
                    }
                }
                   ?>
                    </div>
				</div>
			</div>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>