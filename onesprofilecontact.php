<?php
	session_start();
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="profilecontactstyle.css" type="text/css" />
	<link rel="stylesheet" href="mynameaaboveprofilestyle.css" type="text/css" />
	<script src='https://www.google.com/recaptcha/api.js'>
		
	</script>
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
	?>
	<div class="main">
	
		<div class="leftbar">
			<div class="namelike">
                <div class="name">
               <?php

            if(!isset($_SESSION["name"])){
               echo "Name and Surname";
            } else{
                $name = $_SESSION["name"];
                if($name!=""){
                    echo $name ; 
                } else{
                    echo "Name and Surname";
                }   
            }
                ?>
                </div>
                <div class="update">
                    <a href="profileform.php"><button >Update</button></a>
                </div>
            </div>
			<div class="content">
				<div class="bottomprofile">
					<div class="namelike">
						<div class="name">Wojciech Lamperski</div>
						<div class="like">Like<a href="#"><i class="icon-thumbs-up"></i></a></div>
					</div>
					<div class="date">joined: 07.28.2019</div>
					<div class="linksandvery">
						<div class="nocaptcha">
							<div class="contact">CONTACT: email@gmail.com</div>
							<div class="info">LINKS:</div>
							<div class="info"><a href="">facebook</a></div>
							<div class="info"><a href="">twitter</a></div>
							<div class="info"><a href="">instagram</a></div>
							<div class="info"><a href="">my site</a></div>
						</div>
						<div class="g-recaptcha" data-sitekey="6LciYq0UAAAAAC7gAIAHLI-GJscw1_N0-IasVxZr"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>