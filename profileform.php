<?php
	session_start();
	
	if (!isset($_SESSION['userinfo'])){
		header('Location: login.php');
		exit();
	} else{
        $userinfo = $_SESSION['userinfo'];
    }
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="profileformstyle.css" type="text/css" />
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
	?>
	<div class="main">
	
		<div class="leftbar">
			<?php
				require "profileheader.php";
			?>
			<form action="includes/profileform.inc.php" method="POST">
				<div class="content">
					<div class="bottomprofile">
						<div class="namelike">
							<div class="name"><input type="text" placeholder="Name and Surname" onfocus="this.placeholder=''" onblur="this.placeholder='Name and Surname'" maxlength="30" value="<?php
                                 if(!isset($userinfo['name'])){
                                    echo "";
                                 } else{
                                     if($userinfo['name']!=""){
                                        echo $userinfo['name'];
                                     } else{
                                        echo ""; 
                                     }
                                 }
                            ?>" name="name_desc"></div>
							<div class="like"></div>
						</div>
						<div class="uid">
							<?php	
								 if(isset($userinfo['uidUsers'])) {
									echo $userinfo['uidUsers'];
								} else{
									header("Location: ../login.php");
									exit();
								}
							?>
						</div>
						<div class="about">
							<textarea class="longInput" maxlength="300"  cols="30" rows="10" placeholder="<?php 
                                if(!isset($userinfo['description'])){
                                    echo 'Description - Here a person can write what ones field of intrest is, what one is likely to write about etc. All the other information like links to diffrent social media should be put in contact section. ';
                                } else{
                                    if($userinfo['description']!=""){
                                        echo $userinfo['description'];
                                    } else{
                                        echo 'Description - Here a person can write what ones field of intrest is, what one is likely to write about etc. All the other information like links to diffrent social media should be put in contact section. ';
                                    }
                                }
                            ?>" name="description" onfocus="this.placeholder=''" onblur="this.placeholder='Description - Here a person can write what ones field of intrest is, what one is likely to write about etc. All the other information like links to diffrent social media should be put in contact section. '"><?php if(!isset($userinfo['description'])){
                                echo "";
                            } else{
                                if($userinfo['description']!=""){
                                    echo $userinfo['description'];
                                } else{
                                    echo "";
                                }  
                            }?></textarea>
						</div>
						<div class="submit">
							<a href="myprofile.php"><input type="button" value="Cancel"></a>
							<input type="submit" name="info-submit" value="Submit">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>