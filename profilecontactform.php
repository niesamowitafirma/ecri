<?php
	session_start();
	
	if (!isset($_SESSION['userinfo'])){
		header('Location: login.php');
		exit();
	} else{
       $userinfo = $_SESSION['userinfo']; 
    }
?>
<!DOCTYPE HTML>
<html lang="">

<head>
	<?php
		require "settings.php";
	?>
	<title>ecri</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" href="profilecontactformstyle.css" type="text/css" />
</head>

<body>
	<?php
		require "header.php";
		require "mainbackground.php";
    ?>
	<div class="main">
	
		<div class="leftbar">
			<?php
				require "profileheader.php";
			?>
			<div class="content">
				<div class="bottomprofile">
					<div class="namelike">
						<div class="name">Wojciech Lamperski</div>
					</div>
					<div class="date">joined: 07.28.2019</div>
                    <form action="includes/profilecontactform.inc.php" method="POST">
                        <div class="linksmail">
                            <div class="nocaptcha">
                                <div class="contact">
                                   <input placeholder=
                                        "<?php
                                            if(!isset($_SESSION['wrongcontactmail'])){ echo "contact mail";}else{echo " ";} 
                                        ?>" 
                                        value=
                                        "<?php
                                            if(isset($_SESSION['wrongcontactmail'])){
                                                $cmail=$_SESSION['wrongcontactmail']; echo $cmail; unset($_SESSION['wrongcontactmail']);
                                            } else if(isset($_SESSION['correctontactmail'])){
                                                $cmail=$_SESSION['correctontactmail']; echo $cmail; unset($_SESSION['correctontactmail']);
                                            }else if($userinfo!=""){
                                                echo $userinfo['contactmail'];
                                            }else{
                                                echo "";
                                            }
                                         ?>" 
                                         type="text" name="contactmail" placeholder=" contact mail">
                                        <div>
                                            <?php if(isset($_SESSION['templinkname1']) || isset($_SESSION['templink1']) || isset($_SESSION['templinkname2']) || isset($_SESSION['templink2']) || isset($_SESSION['templinkname3']) || isset($_SESSION['templink3']) || isset($_SESSION['templinkname4']) || isset($_SESSION['templink4']) || isset($_SESSION['templinkname5']) || isset($_SESSION['templink5']) || isset($_SESSION['templinkname6']) || isset($_SESSION['templink6'])){
                                                echo "Please fill both name and link."; 
                                            }else if(isset($_SESSION['wronglink1']) || isset($_SESSION['wronglink2']) || isset($_SESSION['wronglink3']) || isset($_SESSION['wronglink4']) || isset($_SESSION['wronglink5']) || isset($_SESSION['wronglink6'])){
                                                echo "The link you submited is in a wrong format."; 
                                            } else{
                                                echo "";
                                            } ?>
                                    </div> 
                                </div>
                                <div class="info">LINKS:</div>
                                <!--Here are link and linkname inputs in number of 6, they will display values refused, as well as the values that were accepted after submission.--> 
                                <div class="info"><input placeholder=" name" type="text" name="linkname1" onfocus="this.placeholder=''" onblur="this.placeholder='name'" value=
                                "<?php if(isset($_SESSION['templinkname1'])){$templinkname1=$_SESSION['templinkname1']; echo $templinkname1; unset($_SESSION['templinkname1']);}else if(isset($_SESSION['wronglink1'])){$templinkname1=$_SESSION['wronglink1']; echo $templinkname1;} else if($userinfo!=""){echo $userinfo['linkname1'];}else{echo "";}?>"><input placeholder="<?php if(!isset($_SESSION['wronglink1'])){ echo "link";}else{echo " ";} ?>" type="text" name="link1" onfocus="this.placeholder=''" onblur="this.placeholder='link'" value="<?php if(isset($_SESSION['templink1'])){$templink1=$_SESSION['templink1']; echo $templink1; unset($_SESSION['templink1']);} else if(isset($_SESSION['wronglink1'])){echo ""; unset($_SESSION['wronglink1']);} else if($userinfo!=""){echo $userinfo['link1'];}else{echo "";}?>">
                                </div>
                                
                                <div class="info"><input placeholder=" name" type="text" name="linkname2" onfocus="this.placeholder=''" onblur="this.placeholder='name'" value="<?php if(isset($_SESSION['templinkname2'])){$templinkname2=$_SESSION['templinkname2']; echo $templinkname2; unset($_SESSION['templinkname2']);}else if(isset($_SESSION['wronglink2'])){$templinkname2=$_SESSION['wronglink2']; echo $templinkname2;} else if($userinfo!=""){echo $userinfo['linkname2'];}else{echo "";}?>"><input placeholder="<?php if(!isset($_SESSION['wronglink2'])){ echo "link";}else{echo " ";} ?>" type="text" name="link2" onfocus="this.placeholder=''" onblur="this.placeholder='link'" value="<?php if(isset($_SESSION['templink2'])){$templink2=$_SESSION['templink2']; echo $templink2; unset($_SESSION['templink2']);} else if(isset($_SESSION['wronglink2'])){echo ""; unset($_SESSION['wronglink2']);} else if($userinfo!=""){echo $userinfo['link2'];}else{echo "";}?>">
                                </div>
                                
                                <div class="info"><input placeholder=" name" type="text" name="linkname3" onfocus="this.placeholder=''" onblur="this.placeholder='name'" value="<?php if(isset($_SESSION['templinkname3'])){$templinkname3=$_SESSION['templinkname3']; echo $templinkname3; unset($_SESSION['templinkname3']);}else if(isset($_SESSION['wronglink3'])){$templinkname3=$_SESSION['wronglink3']; echo $templinkname3;} else if($userinfo!=""){echo $userinfo['linkname3'];}else{echo "";}?>"><input placeholder="<?php if(!isset($_SESSION['wronglink3'])){ echo "link";}else{echo " ";} ?>" type="text" name="link3" onfocus="this.placeholder=''" onblur="this.placeholder='link'" value="<?php if(isset($_SESSION['templink3'])){$templink3=$_SESSION['templink3']; echo $templink3; unset($_SESSION['templink3']);} else if(isset($_SESSION['wronglink3'])){echo ""; unset($_SESSION['wronglink3']);} else if($userinfo!=""){echo $userinfo['link3'];}else{echo "";}?>">
                                </div>
                                
                                <div class="info"><input placeholder=" name" type="text" name="linkname4" onfocus="this.placeholder=''" onblur="this.placeholder='name'" value="<?php if(isset($_SESSION['templinkname4'])){$templinkname4=$_SESSION['templinkname4']; echo $templinkname4; unset($_SESSION['templinkname4']);}else if(isset($_SESSION['wronglink4'])){$templinkname4=$_SESSION['wronglink4']; echo $templinkname4;} else if($userinfo!=""){echo $userinfo['linkname4'];}else{echo "";}?>"><input placeholder="<?php if(!isset($_SESSION['wronglink4'])){ echo "link";}else{echo " ";} ?>" type="text" name="link4" onfocus="this.placeholder=''" onblur="this.placeholder='link'" value="<?php if(isset($_SESSION['templink4'])){$templink4=$_SESSION['templink4']; echo $templink4; unset($_SESSION['templink4']);} else if(isset($_SESSION['wronglink4'])){echo ""; unset($_SESSION['wronglink4']);} else if($userinfo!=""){echo $userinfo['link4'];}else{echo "";}?>">
                                </div>
                                
                                <div class="info"><input placeholder=" name" type="text" name="linkname5" onfocus="this.placeholder=''" onblur="this.placeholder='name'" value="<?php if(isset($_SESSION['templinkname5'])){$templinkname5=$_SESSION['templinkname5']; echo $templinkname5; unset($_SESSION['templinkname5']);}else if(isset($_SESSION['wronglink5'])){$templinkname5=$_SESSION['wronglink5']; echo $templinkname5;} else if($userinfo!=""){echo $userinfo['linkname5'];}else{echo "";}?>"><input placeholder="<?php if(!isset($_SESSION['wronglink5'])){ echo "link";}else{echo " ";} ?>" type="text" name="link5" onfocus="this.placeholder=''" onblur="this.placeholder='link'" value="<?php if(isset($_SESSION['templink5'])){$templink5=$_SESSION['templink5']; echo $templink5; unset($_SESSION['templink5']);} else if(isset($_SESSION['wronglink5'])){echo ""; unset($_SESSION['wronglink5']);} else if($userinfo!=""){echo $userinfo['link5'];}else{echo "";}?>">
                                </div>
                                
                                <div class="info"><input placeholder=" name" type="text" name="linkname6" onfocus="this.placeholder=''" onblur="this.placeholder='name'" value="<?php if(isset($_SESSION['templinkname6'])){$templinkname6=$_SESSION['templinkname6']; echo $templinkname6; unset($_SESSION['templinkname6']);}else if(isset($_SESSION['wronglink6'])){$templinkname6=$_SESSION['wronglink6']; echo $templinkname6;} else if($userinfo!=""){echo $userinfo['linkname6'];}else{echo "";}?>"><input placeholder="<?php if(!isset($_SESSION['wronglink6'])){ echo "link";}else{echo " ";} ?>" type="text" name="link6" onfocus="this.placeholder=''" onblur="this.placeholder='link'" value="<?php if(isset($_SESSION['templink6'])){$templink6=$_SESSION['templink6']; echo $templink6; unset($_SESSION['templink6']);} else if(isset($_SESSION['wronglink6'])){echo ""; unset($_SESSION['wronglink6']);} else if($userinfo!=""){echo $userinfo['link6'];}else{echo "";}?>">
                                </div>
                            </div>
                        </div>
                        <div class="submit">
                            <a href="myprofilecontact.php"><input type="button" value="Cancel"></a>
                            <input type="submit" name="info-submit" value="Submit">
                        </div>
                    </form>
				</div>
			</div>
		</div>
	</div>
		<?php
			require "rightbar.php";
		?>
</body>

</html>